
# Installing Prometheus and Grafana on EKS

### Prerequisites:
- AWS CLI configured with necessary permissions.
- `kubectl` installed and configured to interact with Amazon EKS clusters.
- Helm installed to deploy prometheus and grafana using charts.


## Automation


### Setup
To set up the environment, run:
```bash
setup.bash
```

### Cleaning
To clean up the environment, run:
```bash
clean.bash
```

## Manual Steps

### Step 1: Setting up `kubectl` to work with AWS EKS
```bash
aws eks --region us-west-2 update-kubeconfig --name final-project-cluster
```

### Step 2: Create a namespace for observation
```bash
kubectl create ns observation
```

### Step 3: Set default namespace
```bash
kubectl config set-context --current --namespace=observation
```

### Step 4: Adding the Prometheus and Grafana Helm repos
```bash
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo add grafana https://grafana.github.io/helm-charts
helm repo update
```

### Step 5: Installing Prometheus with the `prometheus-values.yaml` file
```bash
helm install prometheus prometheus-community/prometheus -f prometheus-values.yaml
```

### Step 6: Installing Grafana with the `grafana-values.yaml` file
```bash
helm install grafana grafana/grafana -f grafana-values.yaml
```

### Step 7: Get the password for Grafana (username is `admin`)
```bash
kubectl get secret --namespace observation grafana -o jsonpath="{.data.admin-password}" | base64 --decode ; echo
```

### Step 8: Connecting Prometheus to Grafana
![1](Images/login-page-grafana.PNG)

![2](Images/home-page-grafana.PNG)

![3](Images/connect-1.PNG)

![4](Images/connect-2.PNG)

![5](Images/connect-3.PNG)

![6](Images/connect-4.PNG)

![7](Images/connect-5.PNG)

![8](Images/connect-6.PNG)

![9](Images/connect-7.PNG)

![10](Images/connect-8.PNG)

![11](Images/connect-9.PNG)

### Cleaning Up
```bash
kubectl delete all --all -n observation

helm uninstall grafana
helm uninstall prometheus

helm repo remove grafana
helm repo remove prometheus-community

kubectl delete ns observation
```
