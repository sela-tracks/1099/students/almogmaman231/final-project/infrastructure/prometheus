aws eks --region us-west-2 update-kubeconfig --name final-project-cluster
kubectl create ns observation
kubectl config set-context --current --namespace=observation

helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo add grafana https://grafana.github.io/helm-charts
helm repo update


helm install prometheus prometheus-community/prometheus -f prometheus-values.yaml


helm install grafana grafana/grafana -f grafana-values.yaml
sleep 30
kubectl get secret --namespace observation grafana -o jsonpath="{.data.admin-password}" | base64 --decode ; echo